package com.`as`.notes

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.NavController
import androidx.navigation.fragment.findNavController
import com.`as`.notes.databinding.FragmentAddNoteBinding
import com.`as`.notes.db.Note
import com.`as`.notes.viewmodel.NotesViewModel
import java.util.Calendar


class AddNoteFragment : Fragment() {
    private lateinit var binding: FragmentAddNoteBinding
    private lateinit var navController: NavController
    private lateinit var viewModel: NotesViewModel


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {

        binding = FragmentAddNoteBinding.inflate(inflater, container, false)

        navController = findNavController()

        viewModel = ViewModelProvider(this)[NotesViewModel::class.java]

        binding.saveBtn.setOnClickListener {
            val name = binding.enterNoteName.text.toString()
            val content = binding.enterNoteContent.text.toString()
            val date = Calendar.getInstance().time.toString()

            val eventDetails = Bundle()
            eventDetails.putString("add_note_message", "New note is added")
            mFirebaseAnalytics.logEvent("add_note", eventDetails)

            viewModel.addNote(Note(name, content, date))
            navController.navigateUp()
        }

        return binding.root
    }
}