package com.`as`.notes

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.NavController
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.`as`.notes.databinding.FragmentAddNoteBinding
import com.`as`.notes.db.Note
import com.`as`.notes.viewmodel.NotesViewModel
import java.util.Calendar

class EditNoteFragment : Fragment() {
    private lateinit var binding: FragmentAddNoteBinding
    private lateinit var navController: NavController
    private lateinit var viewModel: NotesViewModel
    private val args: EditNoteFragmentArgs by navArgs()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentAddNoteBinding.inflate(inflater, container, false)

        navController = findNavController()

        viewModel = ViewModelProvider(this)[NotesViewModel::class.java]

        binding.enterNoteName.isEnabled = false
        binding.enterNoteName.setText(args.noteName)

        binding.saveBtn.setOnClickListener {
            val content = binding.enterNoteContent.text.toString()
            val date = Calendar.getInstance().time.toString()

            viewModel.updateNote(Note(args.noteName, content, date, id =  args.noteId))
            navController.popBackStack(R.id.notesListFragment, false)
        }

        return binding.root
    }
}