package com.`as`.notes

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.NavController
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.`as`.notes.adapter.RecyclerViewAdapter
import com.`as`.notes.databinding.FragmentNotesListBinding
import com.`as`.notes.viewmodel.NotesViewModel

class NotesListFragment : Fragment() {

    private lateinit var viewModel: NotesViewModel
    private lateinit var binding: FragmentNotesListBinding
    private lateinit var adapter: RecyclerViewAdapter
    private lateinit var navController: NavController

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = FragmentNotesListBinding.inflate(inflater, container, false)
        binding.recyclerView.layoutManager = LinearLayoutManager(requireContext())

        navController = findNavController()

        adapter = RecyclerViewAdapter(requireContext(), onClick = {
            val eventDetails = Bundle()
            eventDetails.putString("view_note_message", "The note is viewed")
            mFirebaseAnalytics.logEvent("view_note", eventDetails)
            val action = NotesListFragmentDirections.actionDetails(it.id, it.name, it.content)
            navController.navigate(action)
        })

        binding.recyclerView.adapter = adapter

        viewModel = ViewModelProvider(this)[NotesViewModel::class.java]
        viewModel.allNotes.observe(
            requireActivity(),
            Observer { list -> list?.let { adapter.updateList(it) } })

        binding.createNoteBtn.setOnClickListener {
            val action = NotesListFragmentDirections.actionAddNote()
            navController.navigate(action)
        }

        return binding.root
    }
}