package com.`as`.notes.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.`as`.notes.databinding.ItemBinding
import com.`as`.notes.db.Note

class RecyclerViewAdapter(private val context: Context, private val onClick: (Note)->Unit) :
    RecyclerView.Adapter<RecyclerViewAdapter.ViewHolder>() {
    private var notesList = mutableListOf<Note>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val inflater = LayoutInflater.from(context)
        val binding = ItemBinding.inflate(inflater, parent, false)
        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val note = notesList[position]
        holder.name.text = note.name
        holder.date.text = note.date

        holder.itemView.setOnClickListener { onClick(note) }
    }

    fun updateList(newList: List<Note>) {
        notesList.clear()
        notesList.addAll(newList)
        notifyDataSetChanged()
    }

    override fun getItemCount() = notesList.size

    inner class ViewHolder(binding: ItemBinding) :
        RecyclerView.ViewHolder(binding.root) {
        val name = binding.name
        val date = binding.date
    }
}
