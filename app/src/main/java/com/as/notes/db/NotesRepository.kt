package com.`as`.notes.db

import androidx.lifecycle.LiveData

class NotesRepository(private val notesDao: NotesDAO) {
    //fun allNotes(): LiveData<List<Note>> = notesDao.getAll()
    val allNotes: LiveData<List<Note>> = notesDao.getAll()

    fun insert(note: Note) {
        notesDao.insert(note)
    }

    fun delete(id: Int){
        notesDao.delete(id)
    }

    fun update(note: Note){
        notesDao.update(note)
    }
}
