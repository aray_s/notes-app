package com.`as`.notes

import android.content.Context
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.NavController
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.`as`.notes.databinding.FragmentDetailsBinding
import com.`as`.notes.viewmodel.NotesViewModel
import com.google.firebase.analytics.FirebaseAnalytics

class DetailsFragment : Fragment() {
    private lateinit var binding: FragmentDetailsBinding
    private lateinit var navController: NavController
    private lateinit var viewModel: NotesViewModel
    private val args: DetailsFragmentArgs by navArgs()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentDetailsBinding.inflate(inflater, container, false)

        navController = findNavController()

        viewModel = ViewModelProvider(this)[NotesViewModel::class.java]

        binding.noteName.text = args.noteName
        binding.noteContent.text = args.noteContent

        binding.deleteNoteBtn.setOnClickListener {
            viewModel.deleteNote(args.noteId)
            val eventDetails = Bundle()
            eventDetails.putString("remove_note_message", "The note is removed")
            mFirebaseAnalytics.logEvent("remove_note", eventDetails)
            navController.navigateUp()
        }

        binding.editNoteBtn.setOnClickListener {
            val action = DetailsFragmentDirections.actionEditNote(args.noteId, args.noteName)
            navController.navigate(action)
        }

        return binding.root
    }

}